CREATE DATABASE IF NOT EXISTS dev_book;
USE dev_book;

-- DROP TABLE IF EXISTS publicacoes;
-- DROP TABLE IF EXISTS seguidores;
DROP TABLE IF EXISTS users;

CREATE TABLE users(
    id SERIAL primary key,
    name varchar(50) not null,
    username varchar(50) not null unique,
    email varchar(50) not null unique,
    password varchar(100) not null,
    created_at timestamp default current_timestamp
);


CREATE TABLE followers(
    user_id int not null,
    FOREIGN KEY (user_id)
    REFERENCES users(id)
    ON DELETE CASCADE,
   	follower_id int not null,
    FOREIGN KEY (follower_id)
    REFERENCES users(id)
    ON DELETE CASCADE,
    primary key(user_id, follower_id)
);

CREATE TABLE publications(
    id SERIAL primary key,
    title varchar(50) not null,
    description varchar(50) not null,
    author_id int not null,
  	likes int DEFAULT 0,
    created_at timestamp default current_timestamp,
  	
  	FOREIGN KEY (author_id)
  	REFERENCES users(id)
  	ON DELETE CASCADE
);
