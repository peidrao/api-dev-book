package main

import (
	"api-dev-book/src/config"
	"api-dev-book/src/router"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

// func init() {
// 	key := make([]byte, 64)

// 	if _, err := rand.Read(key); err != nil {
// 		log.Fatal(err)
// 	}

// 	keyBase64 := base64.StdEncoding.EncodeToString(key)
// 	fmt.Println(keyBase64)
// }

func main() {
	config.Config()
	r := router.Build()

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.PORT), r))
}
