module api-dev-book

go 1.13

require (
	github.com/badoux/checkmail v1.2.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.6
	github.com/techschool/simplebank v0.0.0-20220429190511-1ab553c99bb2
	golang.org/x/crypto v0.0.0-20220408190544-5352b0902921
)
