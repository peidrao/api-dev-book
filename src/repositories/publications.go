package repositories

import (
	"api-dev-book/src/models"
	"database/sql"
)

type Publications struct {
	db *sql.DB
}

func NewRepositoryPublications(db *sql.DB) *Publications {
	return &Publications{db}
}

func (p Publications) CreatePublicationsRepository(publication models.Publication) (uint64, error) {
	var id uint64
	err := p.db.QueryRow(`
		INSERT INTO publications (title, description, author_id) 
		VALUES ($1, $2, $3) RETURNING id`,
		publication.Title, publication.Description, publication.AuthorID).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (p Publications) GetPublicationRepository(id uint64) (models.Publication, error) {
	line, err := p.db.Query(`
		SELECT p.*, u.username 
		FROM publications p  INNER JOIN users u 
		ON u.id = p.author_id WHERE p.id = $1`, id)

	if err != nil {
		return models.Publication{}, err
	}

	defer line.Close()
	var publication models.Publication

	if line.Next() {
		if err = line.Scan(
			&publication.ID,
			&publication.Title,
			&publication.Description,
			&publication.AuthorID,
			&publication.Likes,
			&publication.CreateAt,
			&publication.AuthorUsername); err != nil {
			return models.Publication{}, err
		}
	}

	return publication, err
}

// Ver publicações de que o usuário segue e suas próprias publicações
func (p Publications) GetPublicationsRepository(id uint64) ([]models.Publication, error) {
	lines, err := p.db.Query(`
		SELECT DISTINCT p.*, u.username 
		FROM publications p 
		INNER JOIN users u 
		ON u.id = p.author_id
		INNER JOIN followers f 
		on f.follower_id = $1
		WHERE p.author_id = $2 or p.author_id = f.user_id
		ORDER BY 1 DESC`, id, id)

	if err != nil {
		return []models.Publication{}, err
	}

	defer lines.Close()
	var publications []models.Publication

	for lines.Next() {
		var publication models.Publication
		if err = lines.Scan(
			&publication.ID,
			&publication.Title,
			&publication.Description,
			&publication.AuthorID,
			&publication.Likes,
			&publication.CreateAt,
			&publication.AuthorUsername); err != nil {
			return []models.Publication{}, err
		}
		publications = append(publications, publication)
	}

	return publications, nil
}

func (p Publications) UpdatePublicationsRepository(publicationID uint64, publication models.Publication) error {
	statement, err := p.db.Prepare(`
		UPDATE publications 
		SET title = $1, description = $2
		WHERE id = $3
		`)

	if err != nil {
		return err
	}

	defer statement.Close()

	if _, err = statement.Exec(publication.Title, publication.Description, publicationID); err != nil {
		return err
	}
	return nil
}

func (p Publications) DestroyPublicationRepository(id uint64) error {
	line, err := p.db.Prepare(`DELETE FROM publications WHERE id = $1`)

	if err != nil {
		return err
	}

	defer line.Close()
	if _, err = line.Exec(id); err != nil {
		return err
	}

	return nil
}

func (p Publications) GetPublicationUserRepository(userID uint64) ([]models.Publication, error) {
	lines, err := p.db.Query(`
		SELECT p.*, u.username 
		FROM publications p 
		JOIN users u 
		ON u.id = p.author_id
		WHERE p.author_id = $1
		ORDER BY 1 DESC`, userID)

	if err != nil {
		return []models.Publication{}, err
	}

	defer lines.Close()
	var publications []models.Publication

	for lines.Next() {
		var publication models.Publication
		if err = lines.Scan(
			&publication.ID,
			&publication.Title,
			&publication.Description,
			&publication.AuthorID,
			&publication.Likes,
			&publication.CreateAt,
			&publication.AuthorUsername); err != nil {
			return []models.Publication{}, err
		}
		publications = append(publications, publication)
	}

	return publications, nil
}

func (p Publications) LikePublicationRepository(publicationID uint64) error {
	statement, err := p.db.Prepare(`
		UPDATE publications 
		SET likes = likes +1
		WHERE id = $1
		`)

	if err != nil {
		return err
	}

	defer statement.Close()

	if _, err = statement.Exec(publicationID); err != nil {
		return err
	}
	return nil
}

func (p Publications) DisLikePublicationRepository(publicationID uint64) error {
	statement, err := p.db.Prepare(`
		UPDATE publications 
		SET likes = 
		CASE WHEN likes > 0 THEN likes - 1
		ELSE likes END
		WHERE id = $1
		`)

	if err != nil {
		return err
	}

	defer statement.Close()

	if _, err = statement.Exec(publicationID); err != nil {
		return err
	}
	return nil
}
