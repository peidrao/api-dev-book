package repositories

import (
	"api-dev-book/src/models"
	"database/sql"
	"fmt"
)

type Users struct {
	db *sql.DB
}

func NewRepositoryUser(db *sql.DB) *Users {
	return &Users{db}
}

func (u Users) Create(user models.User) (uint64, error) {
	var id uint64
	err := u.db.QueryRow("insert into users (name, username, email, password) values($1, $2, $3, $4) returning id",
		user.Name, user.Username, user.Email, user.Password).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (u Users) GetAllOrQuery(usernameOrName string) ([]models.User, error) {
	usernameOrName = fmt.Sprintf("%%%s%%", usernameOrName) // %usernameOrName%
	lines, err := u.db.Query("select id, name, username, email, created_at from users where username like $1 or name like $2", usernameOrName, usernameOrName)

	if err != nil {
		return nil, err
	}
	defer lines.Close()
	var users []models.User

	for lines.Next() {
		var user models.User
		if err = lines.Scan(
			&user.ID, &user.Name, &user.Username, &user.Email, &user.CreatedAt,
		); err != nil {
			return nil, err
		}

		users = append(users, user)
	}
	return users, nil
}

func (u Users) Get(id uint64) (models.User, error) {
	line, err := u.db.Query("select id, name, username, email, created_at from users where id = $1", id)

	if err != nil {
		return models.User{}, err
	}
	defer line.Close()
	var user models.User

	if line.Next() {
		if err = line.Scan(&user.ID, &user.Name, &user.Username, &user.Email, &user.CreatedAt); err != nil {
			return models.User{}, err
		}
	}

	return user, err
}

func (u Users) Update(id uint64, user models.User) error {
	statement, err := u.db.Prepare("update users set username = $1, name = $2, email = $3 where id = $4")

	if err != nil {
		return err
	}

	defer statement.Close()

	if _, err = statement.Exec(user.Username, user.Name, user.Email, id); err != nil {
		return err
	}
	return nil
}

func (u Users) Destroy(id uint64) error {
	statement, err := u.db.Prepare("delete from users where id = $1")

	if err != nil {
		return err
	}
	defer statement.Close()

	if _, err = statement.Exec(id); err != nil {
		return err
	}
	return nil
}

func (u Users) GetByEmail(email string) (models.User, error) {
	line, err := u.db.Query("select id, password from users where email = $1", email)

	if err != nil {
		return models.User{}, err
	}
	defer line.Close()
	var user models.User

	if line.Next() {
		if err = line.Scan(&user.ID, &user.Password); err != nil {
			return models.User{}, err
		}
	}

	return user, nil
}

func (u Users) Follow(userID, followerID uint64) error {
	statment, err := u.db.Prepare("INSERT INTO followers(user_id, follower_id) VALUES($1, $2) ON CONFLICT(user_id, follower_id) DO NOTHING")

	if err != nil {
		return err
	}

	defer statment.Close()
	if _, err = statment.Exec(userID, followerID); err != nil {
		return err
	}

	return nil
}

func (u Users) UnFollow(userID, followerID uint64) error {
	statment, err := u.db.Prepare("DELETE FROM followers WHERE user_id = $1 and follower_id = $2")

	if err != nil {
		return err
	}

	defer statment.Close()
	if _, err = statment.Exec(userID, followerID); err != nil {
		return err
	}

	return nil
}

func (u Users) GetFollowers(userID uint64) ([]models.User, error) {
	lines, err := u.db.Query(`
		SELECT u.id, u.name, u.username, u.email, u.created_at 
		FROM users u 
		INNER JOIN followers s 
		ON u.id = follower_id 
		WHERE s.user_id = $1`, userID)

	if err != nil {
		return nil, err
	}

	defer lines.Close()
	var users []models.User

	for lines.Next() {
		var user models.User
		if err = lines.Scan(
			&user.ID, &user.Name, &user.Username, &user.Email, &user.CreatedAt,
		); err != nil {
			return nil, err
		}

		users = append(users, user)
	}
	return users, nil
}

func (u Users) GetFollowings(userID uint64) ([]models.User, error) {
	lines, err := u.db.Query(`
		SELECT u.id, u.name, u.username, u.email, u.created_at 
		FROM users u 
		INNER JOIN followers s 
		ON u.id = user_id 
		WHERE s.follower_id = $1`, userID)

	if err != nil {
		return nil, err
	}

	defer lines.Close()
	var users []models.User

	for lines.Next() {
		var user models.User
		if err = lines.Scan(
			&user.ID, &user.Name, &user.Username, &user.Email, &user.CreatedAt,
		); err != nil {
			return nil, err
		}

		users = append(users, user)
	}
	return users, nil
}

func (u Users) GetPassword(userID uint64) (string, error) {
	line, err := u.db.Query(`
		SELECT password 
		FROM users
		WHERE id = $1`, userID)

	if err != nil {
		return "", err
	}

	defer line.Close()
	var user models.User

	for line.Next() {
		if err = line.Scan(&user.Password); err != nil {
			return "", err
		}
	}
	return user.Password, nil
}

func (u Users) UpdateNewPassword(id uint64, password string) error {
	statement, err := u.db.Prepare("update users set password = $1 where id = $2")

	if err != nil {
		return err
	}

	defer statement.Close()

	if _, err = statement.Exec(password, id); err != nil {
		return err
	}
	return nil
}
