package database

import (
	"api-dev-book/src/config"
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

func Connect() (*sql.DB, error) {
	db, err := sql.Open("postgres", config.DATABASE_URI)
	if err != nil {
		log.Fatal("Cannot connect to db:", err)
	}

	if err = db.Ping(); err != nil {
		db.Close()
		return nil, err
	}
	return db, nil
}
