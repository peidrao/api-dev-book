package models

import (
	"errors"
	"strings"
	"time"
)

type Publication struct {
	ID             uint64    `json:"id,omitempty"`
	Title          string    `json:"title,omitempty"`
	Description    string    `json:"description,omitempty"`
	AuthorID       uint64    `json:"author_id,omitempty"`
	AuthorUsername string    `json:"author_username,omitempty"`
	Likes          uint64    `json:"likes"`
	CreateAt       time.Time `json:"created_at"`
}

func (publication *Publication) PreparePublication() error {
	if err := publication.validate(); err != nil {
		return err
	}

	publication.format()

	return nil
}

func (publication *Publication) validate() error {
	if publication.Title == "" {
		return errors.New("o título da publicação não pode estar em branco")
	}
	if publication.Description == "" {
		return errors.New("a descrição não pode estar em branco")
	}

	return nil
}

func (publication *Publication) format() {
	publication.Title = strings.TrimSpace(publication.Title)
	publication.Description = strings.TrimSpace(publication.Description)
}
