package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var (
	DATABASE_URI = ""
	PORT         = 0
	SECRET_KEY   []byte
)

func Config() {
	var erro error

	if erro = godotenv.Load(); erro != nil {
		log.Fatal(erro)
	}

	PORT, erro = strconv.Atoi(os.Getenv("API_PORT"))

	if erro != nil {
		PORT = 9000
	}

	DATABASE_URI = fmt.Sprintf("postgresql://%s:%s@localhost:5433/%s?sslmode=disable", os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_DATABASE"))
	SECRET_KEY = []byte(os.Getenv("SECRET_KEY"))
}
