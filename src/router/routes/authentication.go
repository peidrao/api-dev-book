package routes

import (
	"api-dev-book/src/controllers"
	"net/http"
)

var routerAuthentication = Route{
	URI:                     "/login",
	Method:                  http.MethodPost,
	Function:                controllers.Login,
	IsRequestAuthentication: false,
}
