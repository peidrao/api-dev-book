package routes

import (
	"api-dev-book/src/middlewares"
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	URI                     string
	Method                  string
	Function                func(http.ResponseWriter, *http.Request)
	IsRequestAuthentication bool
}

func Configure(r *mux.Router) *mux.Router {
	routes := usersRoutes
	routes = append(routes, routerAuthentication)
	routes = append(routes, publicationRoutes...)

	for _, route := range routes {
		if route.IsRequestAuthentication {
			r.HandleFunc(route.URI,
				middlewares.Logger(middlewares.Authentication(route.Function)),
			).Methods(route.Method)
		} else {
			r.HandleFunc(route.URI, middlewares.Logger(route.Function)).Methods(route.Method)
		}

	}
	return r
}
