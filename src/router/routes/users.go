package routes

import (
	"api-dev-book/src/controllers"
	"net/http"
)

var usersRoutes = []Route{
	{
		URI:                     "/users",
		Method:                  http.MethodPost,
		Function:                controllers.CreateUser,
		IsRequestAuthentication: false,
	},
	{
		URI:                     "/users",
		Method:                  http.MethodGet,
		Function:                controllers.GetUsers,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/users/{userId}",
		Method:                  http.MethodGet,
		Function:                controllers.GetUser,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/users/{userId}",
		Method:                  http.MethodPut,
		Function:                controllers.UpdateUser,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/users/{userId}",
		Method:                  http.MethodDelete,
		Function:                controllers.DeleteUser,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/users/{userId}/follow",
		Method:                  http.MethodPost,
		Function:                controllers.FollowUser,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/users/{userId}/unfollow",
		Method:                  http.MethodPost,
		Function:                controllers.UnFollowUser,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/followers/{userID}",
		Method:                  http.MethodGet,
		Function:                controllers.Followers,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/followings/{userId}",
		Method:                  http.MethodGet,
		Function:                controllers.Following,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/update_password",
		Method:                  http.MethodPost,
		Function:                controllers.UpdatePassword,
		IsRequestAuthentication: true,
	},
}
