package routes

import (
	"api-dev-book/src/controllers"
	"net/http"
)

var publicationRoutes = []Route{
	{
		URI:                     "/publications",
		Method:                  http.MethodPost,
		Function:                controllers.CreatePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications",
		Method:                  http.MethodGet,
		Function:                controllers.GetPublications,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publication_id}",
		Method:                  http.MethodGet,
		Function:                controllers.GetPublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publication_id}",
		Method:                  http.MethodPut,
		Function:                controllers.UpdatePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publication_id}",
		Method:                  http.MethodDelete,
		Function:                controllers.DeletePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/users/{userId}/publications",
		Method:                  http.MethodGet,
		Function:                controllers.GetPublicationsByUser,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publication_id}/like",
		Method:                  http.MethodPost,
		Function:                controllers.LikePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publication_id}/dislike",
		Method:                  http.MethodPost,
		Function:                controllers.DisLikePublication,
		IsRequestAuthentication: true,
	},
}
