package router

import (
	"api-dev-book/src/router/routes"

	"github.com/gorilla/mux"
)

func Build() *mux.Router {
	r := mux.NewRouter()
	return routes.Configure(r)
}
