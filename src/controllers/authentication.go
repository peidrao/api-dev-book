package controllers

import (
	"api-dev-book/src/authentication"
	"api-dev-book/src/database"
	"api-dev-book/src/models"
	"api-dev-book/src/repositories"
	"api-dev-book/src/responses"
	"api-dev-book/src/utils"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
)

type AccessToken struct {
	ID    string `json:"id"`
	Token string `json:"token"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.Error(w, http.StatusUnprocessableEntity, err)
	}
	var user models.User
	if err = json.Unmarshal(payload, &user); err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryUser(db)
	user_email, err := repository.GetByEmail(user.Email)

	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}
	if err = utils.CheckPassword(user.Password, user_email.Password); err != nil {
		responses.Error(w, http.StatusUnauthorized, err)
		return
	}
	token, _ := authentication.CreateToken(user_email.ID)
	var ID = strconv.FormatUint(uint64(user_email.ID), 10)
	responses.JSON(w, http.StatusOK, AccessToken{ID: ID, Token: token})
}
