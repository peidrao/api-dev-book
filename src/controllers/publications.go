package controllers

import (
	"api-dev-book/src/authentication"
	"api-dev-book/src/database"
	"api-dev-book/src/models"
	"api-dev-book/src/repositories"
	"errors"
	"strconv"

	"api-dev-book/src/responses"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

func CreatePublication(w http.ResponseWriter, r *http.Request) {
	authorID, err := authentication.ExtractUserID(r)
	if err != nil {
		responses.Error(w, http.StatusUnauthorized, err)
		return
	}

	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.Error(w, http.StatusUnprocessableEntity, err)
		return
	}

	var publication models.Publication
	if err = json.Unmarshal(payload, &publication); err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	publication.AuthorID = authorID

	if err = publication.PreparePublication(); err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)
	publication.ID, err = repository.CreatePublicationsRepository(publication)
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusCreated, publication)
}

func GetPublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publication_id"], 10, 64)
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)

	publication, err := repository.GetPublicationRepository(publicationID)
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}
	if publication.ID == 0 {
		responses.Error(w, http.StatusNotFound, errors.New("publicação não encontrado"))
		return
	}

	responses.JSON(w, http.StatusOK, publication)
}

func GetPublications(w http.ResponseWriter, r *http.Request) {
	authorID, err := authentication.ExtractUserID(r)
	if err != nil {
		responses.Error(w, http.StatusUnauthorized, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)

	publications, err := repository.GetPublicationsRepository(authorID)
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, publications)
}

func UpdatePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publication_id"], 10, 64)
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	authorID, err := authentication.ExtractUserID(r)
	if err != nil {
		responses.Error(w, http.StatusUnauthorized, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)

	publication, err := repository.GetPublicationRepository(publicationID)

	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	if publication.AuthorID != authorID {
		responses.Error(w, http.StatusForbidden, errors.New("não é possível atualizar uma publicação que não é sua"))
		return
	}

	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.Error(w, http.StatusUnprocessableEntity, err)
		return
	}

	var publicationModel models.Publication
	if err = json.Unmarshal(payload, &publicationModel); err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	if err = publicationModel.PreparePublication(); err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	if err = repository.UpdatePublicationsRepository(publicationID, publicationModel); err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusNoContent, nil)
}

func DeletePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationId, err := strconv.ParseUint(params["publication_id"], 10, 64)

	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	authorID, err := authentication.ExtractUserID(r)
	if err != nil {
		responses.Error(w, http.StatusUnauthorized, err)
		return
	}

	repository := repositories.NewRepositoryPublications(db)

	publication, err := repository.GetPublicationRepository(publicationId)

	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	if publication.AuthorID != authorID {
		responses.Error(w, http.StatusForbidden, errors.New("não é possível remover uma publicação que não é sua"))
		return
	}

	if err = repository.DestroyPublicationRepository(publicationId); err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusNoContent, nil)
}

func GetPublicationsByUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userID, err := strconv.ParseUint(params["userId"], 10, 64)

	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)

	publications, err := repository.GetPublicationUserRepository(userID)
	if err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, publications)
}

func LikePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publication_id"], 10, 64)

	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)

	if err := repository.LikePublicationRepository(publicationID); err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusNoContent, nil)
}

func DisLikePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publication_id"], 10, 64)

	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	db, err := database.Connect()
	if err != nil {
		responses.Error(w, http.StatusBadRequest, err)
		return
	}
	defer db.Close()

	repository := repositories.NewRepositoryPublications(db)

	if err := repository.DisLikePublicationRepository(publicationID); err != nil {
		responses.Error(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusNoContent, nil)
}
